/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */
import {
    useBlockProps, InspectorControls,
    // eslint-disable-next-line @wordpress/no-unsafe-wp-apis
    __experimentalPanelColorGradientSettings as PanelColorGradientSettings
} from '@wordpress/block-editor';
import { RawHTML } from '@wordpress/element';
import { useSelect } from '@wordpress/data';

import {
    PanelBody, ToggleControl, QueryControls, TextControl, RangeControl, SelectControl,
    // eslint-disable-next-line @wordpress/no-unsafe-wp-apis
    __experimentalBoxControl as BoxControl
} from '@wordpress/components';
// eslint-disable-next-line no-unused-vars
import { format, dateI18n } from '@wordpress/date';


import RenderCss from "../utils/render-css";
import CustomTypography from "../utils/typography";
import Article from "./template/article"
import ArticleWrapper from "./template/small-wrapper"
/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */


/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */

export default function Edit({ attributes, setAttributes }) {
    const { postType, numberOfPosts, displayFeatureImage, order, orderBy, categories,
        columns, tabColumn, style, showCategory, categoryStyle,
        categoryDevider, enableMeta, enableAuthor, enableExcerptLength, excerptLength,
        enableReadMore, readMoreText, titleColor, titleMargin, titlePadding,
        enableAuthorImage,
        enableDate,
        enableComment,
        enableExcerpt,
        typography



    } = attributes;

    const catIds = categories && categories.length > 0 ? categories.map(cat => cat.id) : [];

    const posts = useSelect(
        (select) => {
            return select('core').getEntityRecords('postType', postType, {
                per_page: numberOfPosts,
                _embed: true,
                order,
                orderby: orderBy,
                categories: catIds
            })
        }, [numberOfPosts, order, orderBy, categories, postType]);


    const categoryList = useSelect((select) => {
        return select('core').getEntityRecords('taxonomy', 'category', {
            per_page: -1
        });
    }, []);

    const catSugessions = {};
    if (categoryList) {
        for (let i = 0; i < categoryList.length; i++) {
            const cat = categoryList[i];
            catSugessions[cat.name] = cat;
        }
    }

    const postTypeList = useSelect((select) => {
        return select('core').getPostTypes();
    }, []);

    const newPostTypes = [];
    if (postTypeList) {
        for (let i = 0; i < postTypeList.length; i++) {
            const post = postTypeList[i];
            newPostTypes.push({
                label: post.name, value: post.slug
            })
        }
    }

    const onCategoryChange = (values) => {
        const hasNoSugessions = values.some((value) => typeof value === 'string' && !catSugessions[value]);

        if (hasNoSugessions) return {};

        const updatedCat = values.map((token) => {
            return typeof token === 'string' ? catSugessions[token] : token;
        });

        setAttributes({ categories: updatedCat });
    }

    const setDisplayFeatureImage = (val) => {
        setAttributes({ displayFeatureImage: val });
    }


    return (
        <><>
            <InspectorControls>

                <PanelBody
                    title={__("Query Settings", 'sparkle-blocks')}
                    initialOpen={true}
                >
                    <SelectControl
                        label="Post Type"
                        value={postType}
                        options={newPostTypes}
                        onChange={(val) => setAttributes({ postType: val })} />
                    <QueryControls
                        numberOfItems={numberOfPosts}
                        onNumberOfItemsChange={(value) => setAttributes({ numberOfPosts: value })}
                        maxItems={10}
                        minItems={2}
                        orderBy={orderBy}
                        onOrderByChange={(value) => setAttributes({ orderBy: value })}
                        order={order}
                        onOrderChange={(value) => setAttributes({ order: value })}

                        categorySuggestions={catSugessions}
                        selectedCategories={categories}
                        onCategoryChange={onCategoryChange} />
                </PanelBody>
                <PanelBody
                    initialOpen={false}
                    title={__("Image Settings", "sparkle-blocks")}>

                    <ToggleControl label={__("Featured Image", 'sparkle-blocks')}
                        checked={displayFeatureImage}
                        onChange={setDisplayFeatureImage} />

                </PanelBody>


                <PanelBody
                    title={__("Layout Settings", "sparkle-blocks")}
                    initialOpen={false}>

                    <RangeControl

                        label={__("Columns", 'sparkle-blocks')}
                        min="1"
                        max="4"
                        value={columns}
                        onChange={(val) => setAttributes({ columns: val })} />
                    <RangeControl
                        label={__("Tablet Columns", 'sparkle-blocks')}
                        min="1"
                        max="4"
                        value={tabColumn}
                        onChange={(val) => setAttributes({ tabColumn: val })} />

                    <SelectControl
                        label="Style"
                        value={style}
                        options={[
                            { label: __('Style 1', 'sparkle-blocks'), value: 'style-1' },
                            { label: __('Style 3', 'sparkle-blocks'), value: 'style-2' },
                            { label: __('Style 3', 'sparkle-blocks'), value: 'style-3' },
                        ]}
                        onChange={(val) => setAttributes({ style: val })} />


                </PanelBody>

                <PanelBody
                    title={__("Category")}
                    initialOpen={false}>
                    <ToggleControl label={__("Show Category", 'sparkle-blocks')}
                        checked={showCategory}
                        onChange={(value) => setAttributes({ showCategory: value })} />
                    {showCategory &&
                        <>
                            <SelectControl
                                label="Category Style"
                                value={categoryStyle}
                                options={[
                                    { label: __('Normal', 'sparkle-blocks'), value: 'normal' },
                                    { label: __('Box', 'sparkle-blocks'), value: 'box' }
                                ]}
                                onChange={(val) => setAttributes({ categoryStyle: val })} />

                            <SelectControl
                                label="Category Divider"
                                value={categoryDevider}
                                options={[
                                    { label: __('|', 'sparkle-blocks'), value: '|' },
                                    { label: __('-', 'sparkle-blocks'), value: '-' },
                                    { label: __('>', 'sparkle-blocks'), value: '>' }
                                ]}
                                onChange={(val) => setAttributes({ categoryDevider: val })} />
                        </>}

                </PanelBody>

                <PanelBody
                    title={__("Meta Settings")}
                    initialOpen={false}>

                    <ToggleControl label={__("Enable Meta Info", 'sparkle-blocks')}
                        checked={enableMeta}
                        onChange={(value) => setAttributes({ enableMeta: value })} />

                    {enableMeta &&

                        <ToggleControl label={__("Enable Author", 'sparkle-blocks')}
                            checked={enableAuthor}
                            onChange={(value) => setAttributes({ enableAuthor: value })} />
                    }
                    {enableMeta && enableAuthor &&
                        <ToggleControl label={__("Enable Author Image", 'sparkle-blocks')}
                            checked={enableAuthorImage}
                            onChange={(value) => setAttributes({ enableAuthorImage: value })} />
                    }
                    {enableMeta &&
                        <ToggleControl label={__("Enable Date", 'sparkle-blocks')}
                            checked={enableDate}
                            onChange={(value) => setAttributes({ enableDate: value })} />
                    }
                    {enableMeta &&
                        <ToggleControl label={__("Enable Comments", 'sparkle-blocks')}
                            checked={enableComment}
                            onChange={(value) => setAttributes({ enableComment: value })} />
                    }

                </PanelBody>
                <PanelBody title={__("Content Settings", "sparkle-blocks")}
                    initialOpen={false}>
                    <ToggleControl label={__("Enable Excerpt", 'sparkle-blocks')}
                        checked={enableExcerpt}
                        onChange={(value) => setAttributes({ enableExcerpt: value })} />
                    <ToggleControl label={__("Enable Excerpt Length", 'sparkle-blocks')}
                        checked={enableExcerptLength}
                        onChange={(value) => setAttributes({ enableExcerptLength: value })} />
                    {enableExcerptLength &&
                        <RangeControl
                            label={__("Excerpt Length", 'sparkle-blocks')}
                            min="10"
                            max="100"
                            value={excerptLength}
                            onChange={(val) => setAttributes({ excerptLength: val })} />}

                    <ToggleControl label={__("Enable Read More", 'sparkle-blocks')}
                        checked={enableReadMore}
                        onChange={(value) => setAttributes({ enableReadMore: value })} />

                    {enableReadMore &&
                        <TextControl label={__("Read More", "sparle-blocks")}
                            val={readMoreText}
                            onChange={(val) => setAttributes({ readMoreText: val })} />
                    }






                </PanelBody>
                <CustomTypography
                    typography={typography}
                    setAttributes={setAttributes}
                ></CustomTypography>

                <PanelBody
                    title={__("Title Settings")}
                    initialOpen={false}
                >
                    <PanelColorGradientSettings
                        title={__("Colors")}
                        gradient={false}
                        settings={[{
                            colorValue: titleColor,
                            gradientValue: attributes.gradientValue1,
                            label: __("Title"),
                            onColorChange: (newValue) => setAttributes({ titleColor: newValue }),
                            onGradientChange: (newValue) => setAttributes({ gradientValue1: newValue }),
                        },
                        {
                            colorValue: attributes.colorValue2,
                            gradientValue: attributes.gradientValue2,
                            colors: [
                                { name: 'red', color: '#f00' },
                                { name: 'white', color: '#fff' },
                                { name: 'blue', color: '#00f' },
                            ],
                            gradients: [
                                {
                                    name: 'Vivid cyan blue to vivid purple',
                                    gradient:
                                        'linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%)',
                                    slug: 'vivid-cyan-blue-to-vivid-purple',
                                },
                                {
                                    name: 'Light green cyan to vivid green cyan',
                                    gradient:
                                        'linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%)',
                                    slug: 'light-green-cyan-to-vivid-green-cyan',
                                },
                                {
                                    name: 'Luminous vivid amber to luminous vivid orange',
                                    gradient:
                                        'linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%)',
                                    slug: 'luminous-vivid-amber-to-luminous-vivid-orange',
                                },
                            ],
                            label: __("Description"),
                            onColorChange: (newValue) => setAttributes({ colorValue2: newValue }),
                            onGradientChange: (newValue) => setAttributes({ gradientValue2: newValue }),
                        }]}
                    >

                    </PanelColorGradientSettings>

                    <BoxControl
                        label={__("Margin", "sparkle-blocks")}
                        values={titleMargin}
                        onChange={(nextValues) => setAttributes({ titleMargin: nextValues })}
                    />
                    <BoxControl
                        label={__("Padding", "sparkle-blocks")}
                        values={titlePadding}
                        onChange={(nextValues) => setAttributes({ titlePadding: nextValues })}

                    />
                </PanelBody>



            </InspectorControls>


        </>

            <RenderCss>
                {`
				.sp-latest-posts-block{
                        &-parent {
                            min-height:200px;
                            padding:30px;
                            div {
                           		h3 a {
                                    font-size: ${typography.desktop.size}px;
                                    line-height: ${typography.desktop.line}px;
                                    letter-spacing: ${typography.desktop.space}px;
                                    font-family: ${typography.desktop.font};
                                    color: ${titleColor};
                                }
                            }
                        }
                `}
            </RenderCss>

            {titleMargin && titlePadding &&
                <RenderCss>
                    {`
                    .sp-latest-posts-block{
                            &-parent {
                                min-height:200px;
                                padding:30px;
                                div {
                                    h3 {
                                    
                                        margin: ${titleMargin.top} ${titleMargin.right} ${titleMargin.bottom} ${titleMargin.left};
                                        padding: ${titlePadding.top} ${titlePadding.right} ${titlePadding.bottom} ${titlePadding.left};
                                    }
                                }
                            }
                    `}

                </RenderCss>
            }
            <div {...useBlockProps({
                className: 'sp-latest-posts-block-parent'
            })}>

                {posts && posts.map((post, index) => {

                    return (
                        <>
                            {index === 0 &&
                                <>
                                    <div className="post-large-block" index={index}>
                                        <Article
                                            post={post}
                                            attributes={attributes}
                                            index={index}
                                        />

                                    </div>

                                    <ArticleWrapper
                                        posts={posts}
                                        attributes={attributes}
                                    />
                                </>
                            }



                        </>
                    );
                })}
            </div></>

    );
}
