
import Article from "./article";


const ArticleWrapper = (props) => {

    const { posts, attributes } = props;

    return (
        <>
            <div className="small-block-wrapper">
                {posts && posts.map((post, index) => {

                    return (
                        <>
                            {index > 0 &&
                                <div className="blog-post-wrapper">
                                    <Article
                                        post={post}
                                        attributes={attributes}
                                        index={index}
                                    />
                                </div>
                            }
                        </>
                    );

                })}
            </div>
        </>
    )
}
export default ArticleWrapper;