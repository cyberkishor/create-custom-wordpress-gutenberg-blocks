import { RawHTML } from '@wordpress/element';

/*Render CSS inside style tag*/
const Article = (props) => {

    const { post, attributes } = props;
    const featuresImage = post._embedded &&
        post._embedded['wp:featuredmedia'] &&
        post._embedded['wp:featuredmedia'].length > 0 &&
        post._embedded['wp:featuredmedia'][0];

    return (
        <>
            {attributes.displayFeatureImage && featuresImage && featuresImage.media_details && featuresImage.media_details.sizes &&
                <figure className='post-thumbnail'>
                    <img src={featuresImage.media_details.sizes.full.source_url}
                        alt={featuresImage.alt} />
                </figure>
            }
            <div className='blog-post-caption'>
                {attributes.showCategory &&
                    <div className='cat-links'>
                        <a href='#'> Asian Glamour</a>
                        <a href='#'> Glamour Award</a>
                    </div>
                }
                <h3>
                    <a href={post.link}>
                        <RawHTML>{post.title.rendered}</RawHTML>
                    </a>
                </h3>

                <div className="user-meta-social-share blog-d-flex blog-align-center blog-justify-space-between">
                    <div className="user-post-meta post-md-size">
                        <a href="https://demo.sparkletheme.com/shiva/blog/author/shiva/" className="user-profile">
                            <span>
                                <img src="https://secure.gravatar.com/avatar/00e9632cd9504427e6f47f0db62b8c3c?s=450&amp;d=mm&amp;r=g" alt="jane-smith" />
                            </span>
                        </a>
                        <div className="user-post-details">
                            <a className="name" href="https://demo.sparkletheme.com/shiva/blog/author/shiva/" title="Shiva">Shiva</a>
                            <div className="meta-content">
                                <span className="post-date">
                                    <span className="date">Mar 11, 2016</span>
                                </span>
                                <span className="reading-time"> <span> <i className="fa fa-clock-o"></i> 2 mins, 38 s</span> </span>
                            </div>
                        </div>


                    </div>
                    <div className="social-share-links">
                        <a href="https://pinterest.com/pin/create/button/?url=https://demo.sparkletheme.com/shiva/blog/2016/03/11/glamour-magazine-goes-to-japan/" className="share-button pinterest" target="_blank" rel="noreferrer"> <i className="fa fa-pinterest-p" aria-hidden="true"></i> </a>

                        <a href="https://www.facebook.com/sharer/sharer.php?u=https://demo.sparkletheme.com/shiva/blog/2016/03/11/glamour-magazine-goes-to-japan/" className="share-button facebook"> <i className="fa fa-facebook" aria-hidden="true"></i></a>

                        <a href="https://twitter.com/intent/tweet?url=https://demo.sparkletheme.com/shiva/blog/2016/03/11/glamour-magazine-goes-to-japan/&amp;text=Glamour Magazine Goes to Japan Mauris Gravida" className="share-button twitter"><i className="fa fa-twitter" aria-hidden="true"></i></a>

                        <a href="mailto:" className="share-button email"><i className="fa fa-envelope-o" aria-hidden="true"></i></a>

                    </div>
                </div>

                {attributes.enableExcerpt && post.excerpt.rendered &&
                    <div className="entry-content">
                        <RawHTML> {post.excerpt.rendered} </RawHTML>
                    </div>
                }

            </div>
        </>
    )
}

export default Article;