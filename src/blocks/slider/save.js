
/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */
import { useBlockProps } from '@wordpress/block-editor';


export default function save(props) {
    const { attributes } = props;

    const locationDisplay = attributes.slides.map((slide) => {
        return (
            <>
                <div
                    className={`slider-item text-${attributes.textAlignment} verticle-${attributes.alignment}`}
                    data-image-id={slide.image_id} data-img-url={slide.image_url}
                    style={{ backgroundImage: `url(${slide.image_url})` }}
                >

                    <div className="slider-caption">
                        <h3>{slide.title} </h3>
                        <p> {slide.description}</p>
                        <a href={slide.button_link}> {slide.button} </a>
                    </div>

                </div>

            </>
        )
    });

    return (

        <div {...useBlockProps.save({
            className: 'sp-slider-block-parent'
        })}>

            {locationDisplay}

        </div>
    );

}
