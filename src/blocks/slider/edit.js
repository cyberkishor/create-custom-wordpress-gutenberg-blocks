/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

import {
    useBlockProps, InspectorControls, RichText, MediaUploadCheck, MediaUpload, BlockControls, BlockVerticalAlignmentToolbar, AlignmentToolbar

} from '@wordpress/block-editor';

import { withSelect } from '@wordpress/data';

import './slide.scss';

import {
    PanelBody, ToggleControl, QueryControls, TextControl, RangeControl, SelectControl, Button, ResponsiveWrapper

} from '@wordpress/components';



const Edit = (props) => {
    const { attributes, setAttributes } = props;
    const handleAddLocation = () => {
        const slides = [...attributes.slides];

        slides.push({
            title: "",
            description: "",
            button: "",
            button_link: "",
            image_url: ""
        });

        setAttributes({ slides });

        jQuery('.sp-slider-block-parent').owlCarousel('refresh');
    };

    const handleRemoveSlide = (index) => {
        const slides = [...attributes.slides];
        slides.splice(index, 1);
        setAttributes({ slides });

        jQuery('.sp-slider-block-parent').owlCarousel('refresh');
    };

    const handleTitleChange = (title, index) => {
        const slides = [...attributes.slides];
        slides[index].title = title;
        setAttributes({ slides });
    };

    const handleDescriptionChange = (desc, index) => {
        const slides = [...attributes.slides];
        slides[index].description = desc;
        setAttributes({ slides });

        jQuery('.sp-slider-block-parent').owlCarousel('refresh');
    };


    const handleBtnChange = (val, index) => {
        const slides = [...attributes.slides];
        slides[index].button = val;
        setAttributes({ slides });

        jQuery('.sp-slider-block-parent').owlCarousel('refresh');
    };

    const handleBtnLinkChange = (val, index) => {
        const slides = [...attributes.slides];
        slides[index].button_url = val;
        setAttributes({ slides });
    };


    const removeMedia = (val, index) => {
        const slides = [...attributes.slides];
        slides[index].image_id = 0;
        slides[index].image_url = '';
        setAttributes({ slides });

        jQuery('.sp-slider-block-parent').owlCarousel('refresh');
    }

    const onSelectMedia = (media, index) => {
        const slides = [...attributes.slides];
        slides[index].image_id = media.id;
        slides[index].image_url = media.url;
        setAttributes({ slides });


        jQuery('.sp-slider-block-parent').owlCarousel('refresh');
    }

    const toggleSlideItem = (event) => {
        jQuery(event.target).parent().find('.slider-item-content').toggleClass('is-open');
    }

    let locationFields,
        locationDisplay;

    if (attributes.slides.length) {
        locationFields = attributes.slides.map((slide, index) => {
            return (
                <>
                    <div className="sp-slider-inspector-wrapper">
                        <div className='slider-item-header' onClick={toggleSlideItem}>
                            <span>{slide.title} </span>
                            <span className="dashicons dashicons-arrow-down-alt2"></span>
                        </div>
                        <div className='slider-item-content'>
                            <TextControl
                                className="slider-title"
                                placeholder="Slider Title"
                                value={slide.title}
                                onChange={(title) => handleTitleChange(title, index)}
                            />

                            <RichText
                                className="slider-description"
                                placeholder="Slider Description"
                                value={slide.description}
                                onChange={(desc) => handleDescriptionChange(desc, index)}
                            />

                            <TextControl
                                className="slider-button"
                                placeholder="Button Text"
                                value={slide.button}
                                onChange={(val) => handleBtnChange(val, index)}
                            />

                            <TextControl
                                className="slider-button-url"
                                placeholder="Button URL"
                                value={slide.button_url}
                                onChange={(val) => handleBtnLinkChange(val, index)}
                            />

                            <div className='image-wrapper'>
                                <MediaUploadCheck>
                                    <MediaUpload
                                        onSelect={(val) => onSelectMedia(val, index)}
                                        value={slide.image_id}
                                        allowedTypes={['image']}
                                        render={({ open }) => (
                                            <Button
                                                className={slide.image_id === 0 ? 'editor-post-featured-image__toggle' : 'editor-post-featured-image__preview'}
                                                onClick={open}
                                            >
                                                {slide.image_id === 0 && __('Choose an image', 'awp')}

                                                {slide.image_url !== '' &&
                                                    <ResponsiveWrapper
                                                        naturalWidth={300} naturalHeight={100}
                                                    >
                                                        <img src={slide.image_url} alt="" />
                                                    </ResponsiveWrapper>
                                                }
                                            </Button>
                                        )}
                                    />
                                </MediaUploadCheck>
                                {slide.image_id !== 0 &&
                                    <MediaUploadCheck>
                                        <MediaUpload
                                            title={__('Replace image', 'awp')}
                                            value={slide.image_id}
                                            onSelect={(media) => onSelectMedia(media, index)}
                                            allowedTypes={['image']}
                                            render={({ open }) => (
                                                <Button className='replace-image' onClick={open} isDefault isLarge>{__('Replace image', 'awp')}</Button>
                                            )}
                                        />
                                    </MediaUploadCheck>
                                }
                                {slide.image_id !== 0 &&
                                    <MediaUploadCheck>
                                        <Button className='remove-image' onClick={(val) => removeMedia(val, index)} isLink isDestructive>
                                            <span className="dashicons dashicons-trash"></span>
                                        </Button>
                                    </MediaUploadCheck>
                                }

                            </div>

                            <Button
                                className="remove-slide"
                                icon="trash"
                                isDestructive
                                label="Delete Slide"
                                onClick={() => handleRemoveSlide(index)}
                            >
                                {__("Remove Slide")}
                            </Button>
                        </div>
                    </div>
                </>
            );
        });

        locationDisplay = attributes.slides.map((slide) => {
            return (
                <>
                    <div
                        className={`slider-item text-${attributes.textAlignment} verticle-${attributes.alignment}`}
                        data-image-id={slide.image_id}
                        data-img-url={slide.image_url}
                        style={{ backgroundImage: `url(${slide.image_url})` }}>

                        <div className="slider-caption">
                            <h3>{slide.title} </h3>
                            <p> {slide.description}</p>
                            <a href={slide.button_link}> {slide.button} </a>
                        </div>

                    </div>

                </>
            )
        });
    }

    return (
        <>
            <BlockControls>
                <BlockVerticalAlignmentToolbar
                    value={attributes.alignment}
                    onChange={(val) => setAttributes({ alignment: val })}
                />
                <AlignmentToolbar
                    value={attributes.textAlignment}
                    default="center"
                    onChange={(val) => setAttributes({ textAlignment: val })}
                />
            </BlockControls>

            <InspectorControls key="1">
                <PanelBody title={__('Slides')}>
                    {locationFields}
                    <Button
                        icon={'plus'}
                        isDefault
                        onClick={handleAddLocation.bind(this)}
                    >
                        {__('Add Slide')}
                    </Button>
                </PanelBody>
            </InspectorControls>

            <div {...useBlockProps({
                className: 'sp-slider-block-parent'
            })}>

                {locationDisplay}

            </div>

        </>
    );

}
export default Edit;