import { __ } from '@wordpress/i18n';
import { useSelect } from '@wordpress/data';

import {
    TabPanel, PanelBody,
    SelectControl,
    RangeControl
} from '@wordpress/components';

import GOOGLE_FONTS from "./fonts";

const SYSTEM_FONTS = [
    { label: __("Default"), value: "default" },
    { label: __("Arial"), value: 'Arial' },
    { label: __("Tahoma"), value: 'Tahoma' },
    { label: __("Verdana"), value: 'Verdana' },
    { label: __("Helvetica"), value: 'Helvetica' },
    { label: __("Times New Roman"), value: 'Times New Roman' },
    { label: __("Trebuchet MS"), value: 'Trebuchet MS' },
    { label: __("Georgia"), value: 'Georgia' }
];

const SYSTEM_FONTS_WEIGHT = [
    { label: __("Default"), value: "default" },
    { label: __("Normal"), value: "normal" },
    { label: __("Bold"), value: "bold" },
    { label: __("100"), value: "100" },
    { label: __("200"), value: "200" },
    { label: __("300"), value: "300" },
    { label: __("400"), value: "400" },
    { label: __("500"), value: "500" },
    { label: __("600"), value: "600" },
    { label: __("700"), value: "700" },
    { label: __("700"), value: "700" },
    { label: __("800"), value: "800" }
];


/*Render CSS inside style tag*/
const CustomTypography = (props) => {

    const { typography, setAttributes } = props;



    let desktopFontTitle = __("Sytem Fonts", 'sparkle-blocks');
    if (typography.desktop.type === 'google') {
        desktopFontTitle = __("Google Fonts", 'sparkle-blocks');
    }

    let tabletFontTitle = __("Sytem Fonts", 'sparkle-blocks');
    if (typography.tablet.type === 'google') {
        tabletFontTitle = __("Google Fonts", 'sparkle-blocks');
    }


    let mobileFontTitle = __("Sytem Fonts", 'sparkle-blocks');
    if (typography.mobile.type === 'google') {
        mobileFontTitle = __("Google Fonts", 'sparkle-blocks');
    }


    const googleFonts = Object.assign({}, GOOGLE_FONTS);
    const gFonts = Object.keys(googleFonts.GOOGLE_FONTS);

    const gFontNames = [];
    for (let i = 0; i < gFonts.length; i++) {
        gFontNames.push({ label: gFonts[i], value: gFonts[i] });
    }


    const desktopSelectedFontWeight = useSelect(() => {
        if (typography.desktop.type === 'google') {
            const selectedFWeight = googleFonts.GOOGLE_FONTS[typography.desktop.font] ? googleFonts.GOOGLE_FONTS[typography.desktop.font].w : [];

            const fWeight = [];
            for (let i = 0; i < selectedFWeight.length; i++) {
                fWeight.push({ label: selectedFWeight[i], value: selectedFWeight[i] });
            }

            return fWeight;
        }
        return SYSTEM_FONTS_WEIGHT;

    }, [typography.desktop.font]);




    const tabletSelectedFontWeight = useSelect(() => {
        if (typography.desktop.type === 'google') {
            const selectedFWeight = googleFonts.GOOGLE_FONTS[typography.tablet.font] ? googleFonts.GOOGLE_FONTS[typography.tablet.font].w : [];

            const fWeight = [];
            for (let i = 0; i < selectedFWeight.length; i++) {
                fWeight.push({ label: selectedFWeight[i], value: selectedFWeight[i] });
            }

            return fWeight;
        }
        return SYSTEM_FONTS_WEIGHT;
    }, [typography.tablet.font]);





    const mobileSelectedFontWeight = useSelect(() => {
        if (typography.mobile.type === 'google') {
            const selectedFWeight = googleFonts.GOOGLE_FONTS[typography.mobile.font] ? googleFonts.GOOGLE_FONTS[typography.mobile.font].w : [];

            const fWeight = [];
            for (let i = 0; i < selectedFWeight.length; i++) {
                fWeight.push({ label: selectedFWeight[i], value: selectedFWeight[i] });
            }

            return fWeight;
        }
        return SYSTEM_FONTS_WEIGHT;
    }, [typography.mobile.font]);


    const loadGfontsUrl = (param) => {

        const link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';

        //link.href = 'http://fonts.googleapis.com/css?family=Oswald&effect=neon';
        document.head.appendChild(link);

        link.href = 'http://fonts.googleapis.com/css?family=' + param.family + '&effect=' + param.effect;


    }


    const onChangeTypoDesktopSize = (val) => {
        const newType = Object.assign({}, typography);
        newType.desktop.size = val;
        setAttributes({ typography: newType });
    }

    const onChangeTypoDesktopType = (val) => {
        const newType = Object.assign({}, typography);
        newType.desktop.type = val;
        setAttributes({ typography: newType });
    }

    const onChangeTypoDesktopFont = (val) => {
        const newType = Object.assign({}, typography);
        newType.desktop.font = val;
        setAttributes({ typography: newType });

        loadGfontsUrl({
            family: val,
            effect: 'neon'
        });
    }
    const onChangeTypoDesktopWeight = (val) => {
        const newType = Object.assign({}, typography);
        newType.desktop.weight = val;

        setAttributes({ typography: newType });
    }

    const setDesktopFontSpace = (val) => {
        const fontSizeCopy = Object.assign({}, typography);
        fontSizeCopy.desktop.space = val;

        setAttributes({ typography: fontSizeCopy });
    }

    const setDesktopFontLine = (val) => {
        const fontSizeCopy = Object.assign({}, typography);
        fontSizeCopy.desktop.line = val;

        setAttributes({ typography: fontSizeCopy });
    }




    const onChangeTypoTabletSize = (val) => {
        const newType = Object.assign({}, typography);
        newType.tablet.size = val;
        setAttributes({ typography: newType });
    }

    const onChangeTypoTabletType = (val) => {
        const newType = Object.assign({}, typography);
        newType.tablet.type = val;
        setAttributes({ typography: newType });
    }

    const onChangeTypoTabletFont = (val) => {
        const newType = Object.assign({}, typography);
        newType.tablet.font = val;
        setAttributes({ typography: newType });
    }
    const onChangeTypoTabletWeight = (val) => {
        const newType = Object.assign({}, typography);
        newType.tablet.weight = val;

        setAttributes({ typography: newType });
    }
    const setTabletFontSpace = (val) => {
        const fontSizeCopy = Object.assign({}, typography);
        fontSizeCopy.tablet.space = val;

        setAttributes({ typography: fontSizeCopy });
    }

    const setTabletFontLine = (val) => {
        const fontSizeCopy = Object.assign({}, typography);
        fontSizeCopy.tablet.line = val;

        setAttributes({ typography: fontSizeCopy });
    }


    const onChangeTypoMobileSize = (val) => {
        const newType = Object.assign({}, typography);
        newType.mobile.size = val;
        setAttributes({ typography: newType });
    }

    const onChangeTypoMobileType = (val) => {
        const newType = Object.assign({}, typography);
        newType.mobile.type = val;
        setAttributes({ typography: newType });
    }

    const onChangeTypoMobileFont = (val) => {
        const newType = Object.assign({}, typography);
        newType.mobile.font = val;
        setAttributes({ typography: newType });
    }
    const onChangeTypoMobileWeight = (val) => {
        const newType = Object.assign({}, typography);
        newType.mobile.weight = val;

        setAttributes({ typography: newType });
    }

    const setMobileFontSpace = (val) => {
        const fontSizeCopy = Object.assign({}, typography);
        fontSizeCopy.mobile.space = val;

        setAttributes({ typography: fontSizeCopy });
    }

    const setMobileFontLine = (val) => {
        const fontSizeCopy = Object.assign({}, typography);
        fontSizeCopy.mobile.line = val;

        setAttributes({ typography: fontSizeCopy });
    }

    const onChangeTypoDesktopCSize = (val) => {
        const newType = Object.assign({}, typography);
        newType.desktop.cSize = val;

        setAttributes({ typography: newType });
    }

    const onChangeTypoTabletCSize = (val) => {
        const newType = Object.assign({}, typography);
        newType.tablet.cSize = val;

        setAttributes({ typography: newType });
    }

    const onChangeTypoMobileCSize = (val) => {
        const newType = Object.assign({}, typography);
        newType.mobile.cSize = val;

        setAttributes({ typography: newType });
    }

    return (
        <>
            <PanelBody
                title={__("Typography", "sprkle-blocks")}>

                <TabPanel
                    className="sp-device-tab-panel"
                    activeClass="active-tab"
                    // onSelect={(v) => console.log(v)}
                    tabs={[
                        {
                            name: 'desktop',
                            title: 'Desktop',
                            className: 'desktop-icon',
                        },
                        {
                            name: 'tablet',
                            title: 'Tablet',
                            className: 'tablet-icon',
                        },
                        {
                            name: 'mobile',
                            title: 'Mobile',
                            className: 'mobile-icon',
                        },
                    ]}
                >
                    {(tab) => {
                        if (tab.name === 'desktop') {
                            return (
                                <>
                                    <SelectControl
                                        label="Font Size"
                                        value={typography.desktop.size}
                                        options={[
                                            { label: "Small", value: "14px" },
                                            { label: "Medium", value: "20px" },
                                            { label: "Large", value: "40px" },
                                            { label: "Custom", value: "custom" },
                                        ]}
                                        onChange={onChangeTypoDesktopSize} />

                                    {typography.desktop.size === 'custom' &&

                                        <RangeControl
                                            label={__("Custom", 'sparkle-blocks')}
                                            min="1"
                                            max="150"
                                            value={typography.desktop.cSize}
                                            onChange={onChangeTypoDesktopCSize}
                                        />
                                    }

                                    <SelectControl
                                        label={__("Type", 'sparkle-blocks')}

                                        value={typography.desktop.type}
                                        options={[
                                            { label: "Default", value: "default" },
                                            { label: "System", value: "system" },
                                            { label: "Google", value: "google" }
                                        ]}
                                        onChange={onChangeTypoDesktopType} />




                                    {
                                        typography.desktop.type && typography.desktop.type !== 'default' &&
                                        <>
                                            {typography.desktop.type === 'system' &&
                                                <SelectControl
                                                    label={desktopFontTitle}

                                                    value={typography.desktop.font}
                                                    options={SYSTEM_FONTS}
                                                    onChange={onChangeTypoDesktopFont} />
                                            }
                                            {typography.desktop.type === 'google' &&
                                                <SelectControl
                                                    label={desktopFontTitle}

                                                    value={typography.desktop.font}
                                                    options={gFontNames}
                                                    onChange={onChangeTypoDesktopFont} />
                                            }

                                            <SelectControl
                                                label={__("Weight", 'sparkle-blocks')}

                                                value={typography.desktop.weight}
                                                options={desktopSelectedFontWeight}
                                                onChange={onChangeTypoDesktopWeight} />

                                        </>
                                    }

                                    <RangeControl
                                        label={__("Line Height", 'sparkle-blocks')}
                                        min="1"
                                        max="150"
                                        value={typography.desktop.line}
                                        onChange={setDesktopFontLine}
                                    />

                                    <RangeControl
                                        label={__("Letter Spacing", 'sparkle-blocks')}
                                        min="-10"
                                        max="20"
                                        value={typography.desktop.space}
                                        onChange={setDesktopFontSpace}
                                    />

                                </>
                            )
                        }

                        if (tab.name === 'tablet') {
                            return (
                                <>
                                    <SelectControl
                                        label="Font Size"
                                        value={typography.tablet.size}
                                        options={[
                                            { label: "Small", value: "14px" },
                                            { label: "Medium", value: "20px" },
                                            { label: "Large", value: "40px" },
                                            { label: "custom", value: "custom" },
                                        ]}
                                        onChange={onChangeTypoTabletSize} />

                                    {typography.tablet.size === 'custom' &&
                                        <RangeControl
                                            label={__("Custom", 'sparkle-blocks')}
                                            min="1"
                                            max="150"
                                            value={typography.mobile.cSize}
                                            onChange={onChangeTypoTabletCSize}
                                        />
                                    }
                                    <SelectControl
                                        label={__("Type", 'sparkle-blocks')}

                                        value={typography.tablet.type}
                                        options={[
                                            { label: "Default", value: "default" },
                                            { label: "System", value: "system" },
                                            { label: "Google", value: "google" }
                                        ]}
                                        onChange={onChangeTypoTabletType} />




                                    {
                                        typography.tablet.type && typography.tablet.type !== 'default' &&
                                        <>
                                            {typography.tablet.type === 'system' &&
                                                <SelectControl
                                                    label={tabletFontTitle}

                                                    value={typography.tablet.font}
                                                    options={SYSTEM_FONTS}
                                                    onChange={onChangeTypoDesktopFont} />
                                            }

                                            {typography.tablet.type === 'google' &&
                                                <SelectControl
                                                    label={tabletFontTitle}

                                                    value={typography.tablet.font}
                                                    options={gFontNames}
                                                    onChange={onChangeTypoTabletFont} />
                                            }
                                            <SelectControl
                                                label={__("Weight", 'sparkle-blocks')}

                                                value={typography.tablet.weight}
                                                options={tabletSelectedFontWeight}
                                                onChange={onChangeTypoTabletWeight} />

                                        </>
                                    }
                                    <RangeControl
                                        label={__("Line Height", 'sparkle-blocks')}
                                        min="1"
                                        max="150"
                                        value={typography.tablet.line}
                                        onChange={setTabletFontLine}
                                    />

                                    <RangeControl
                                        label={__("Letter Spacing", 'sparkle-blocks')}
                                        min="-10"
                                        max="20"
                                        value={typography.tablet.space}
                                        onChange={setTabletFontSpace}
                                    />
                                </>
                            )
                        }

                        if (tab.name === 'mobile') {
                            return (
                                <>
                                    <SelectControl
                                        label="Font Size"
                                        value={typography.mobile.size}
                                        options={[
                                            { label: "Small", value: "14px" },
                                            { label: "Medium", value: "20px" },
                                            { label: "Large", value: "40px" },
                                            { label: "custom", value: "custom" },
                                        ]}
                                        onChange={onChangeTypoMobileSize} />

                                    {typography.mobile.size === 'custom' &&
                                        <RangeControl
                                            label={__("Custom", 'sparkle-blocks')}
                                            min="1"
                                            max="150"
                                            value={typography.mobile.cSize}
                                            onChange={onChangeTypoMobileCSize}
                                        />
                                    }

                                    <SelectControl
                                        label={__("Type", 'sparkle-blocks')}

                                        value={typography.mobile.type}
                                        options={[
                                            { label: "Default", value: "default" },
                                            { label: "System", value: "system" },
                                            { label: "Google", value: "google" }
                                        ]}
                                        onChange={onChangeTypoMobileType} />




                                    {
                                        typography.mobile.type && typography.mobile.type !== 'default' &&
                                        <>
                                            {typography.mobile.type === 'system' &&
                                                <SelectControl
                                                    label={mobileFontTitle}

                                                    value={typography.mobile.font}
                                                    options={SYSTEM_FONTS}
                                                    onChange={onChangeTypoMobileFont} />
                                            }

                                            {typography.mobile.type === 'google' &&
                                                <SelectControl
                                                    label={mobileFontTitle}

                                                    value={typography.mobile.font}
                                                    options={gFontNames}
                                                    onChange={onChangeTypoMobileFont} />
                                            }
                                            <SelectControl
                                                label={__("Weight", 'sparkle-blocks')}

                                                value={typography.mobile.weight}
                                                options={mobileSelectedFontWeight}
                                                onChange={onChangeTypoMobileWeight} />

                                        </>
                                    }
                                    <RangeControl
                                        label={__("Line Height", 'sparkle-blocks')}
                                        min="1"
                                        max="150"
                                        value={typography.mobile.line}
                                        onChange={setMobileFontLine}
                                    />

                                    <RangeControl
                                        label={__("Letter Spacing", 'sparkle-blocks')}
                                        min="-10"
                                        max="20"
                                        value={typography.mobile.space}
                                        onChange={setMobileFontSpace}
                                    />
                                </>
                            )
                        }
                    }
                    }
                </TabPanel>

            </PanelBody>
        </>


    );
};
export default CustomTypography;