export default {
	GOOGLE_FONTS: {
		"ABeeZee":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Abel":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Abhaya Libre":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "sinhala"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Aboreto":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Abril Fatface":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Abyssinica SIL":
		{
			"v": ["regular"],
			"s": ["ethiopic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Aclonica":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Acme":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Actor":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Adamina":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Advent Pro":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["greek", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Aguafina Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Akaya Kanadaka":
		{
			"v": ["regular"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Akaya Telivigala":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Akronim":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Akshar":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Aladin":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alata":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alatsi":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Albert Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Aldrich":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alef":
		{
			"v": ["regular", "700"],
			"s": ["hebrew", "latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Alegreya":
		{
			"v": ["regular", "500", "600", "700", "800", "900", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "900", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Alegreya SC":
		{
			"v": ["regular", "italic", "500", "500italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Alegreya Sans":
		{
			"v": ["100", "100italic", "300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "300", "regular", "500", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Alegreya Sans SC":
		{
			"v": ["100", "100italic", "300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "300", "regular", "500", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Aleo":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Alex Brush":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alfa Slab One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alice":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alike":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alike Angular":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alkalami":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Allan":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Allerta":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Allerta Stencil":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Allison":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Allura":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Almarai":
		{
			"v": ["300", "regular", "700", "800"],
			"s": ["arabic"],
			"w": ["300", "regular", "700", "800"],
			"i": ["normal"]
		},
		"Almendra":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Almendra Display":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Almendra SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Alumni Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Alumni Sans Collegiate One":
		{
			"v": ["regular", "italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Alumni Sans Inline One":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Alumni Sans Pinstripe":
		{
			"v": ["regular", "italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Amarante":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Amaranth":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Amatic SC":
		{
			"v": ["regular", "700"],
			"s": ["cyrillic", "hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Amethysta":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Amiko":
		{
			"v": ["regular", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "600", "700"],
			"i": ["normal"]
		},
		"Amiri":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Amiri Quran":
		{
			"v": ["regular"],
			"s": ["arabic", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Amita":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Anaheim":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Andada Pro":
		{
			"v": ["regular", "500", "600", "700", "800", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Andika":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Anek Bangla":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Devanagari":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Gujarati":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Gurmukhi":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Kannada":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Latin":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Malayalam":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "malayalam"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Odia":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "oriya"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Tamil":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Anek Telugu":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "telugu"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Angkor":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Annie Use Your Telescope":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Anonymous Pro":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "greek", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Antic":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Antic Didone":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Antic Slab":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Anton":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Antonio":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Anybody":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Arapey":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Arbutus":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Arbutus Slab":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Architects Daughter":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Archivo":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Archivo Black":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Archivo Narrow":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Are You Serious":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Aref Ruqaa":
		{
			"v": ["regular", "700"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Aref Ruqaa Ink":
		{
			"v": ["regular", "700"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Arima":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["greek", "greek-ext", "latin", "latin-ext", "malayalam", "tamil", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Arima Madurai":
		{
			"v": ["100", "200", "300", "regular", "500", "700", "800", "900"],
			"s": ["latin", "latin-ext", "tamil", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "700", "800", "900"],
			"i": ["normal"]
		},
		"Arimo":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Arizonia":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Armata":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Arsenal":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Artifika":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Arvo":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Arya":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Asap":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Asap Condensed":
		{
			"v": ["regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Asar":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Asset":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Assistant":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Astloch":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Asul":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Athiti":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Atkinson Hyperlegible":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Atma":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Atomic Age":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Aubrey":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Audiowide":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Autour One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Average":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Average Sans":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Averia Gruesa Libre":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Averia Libre":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Averia Sans Libre":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Averia Serif Libre":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Azeret Mono":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"B612":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"B612 Mono":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"BIZ UDGothic":
		{
			"v": ["regular", "700"],
			"s": ["cyrillic", "greek-ext", "japanese", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"BIZ UDMincho":
		{
			"v": ["regular"],
			"s": ["cyrillic", "greek-ext", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"BIZ UDPGothic":
		{
			"v": ["regular", "700"],
			"s": ["cyrillic", "greek-ext", "japanese", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"BIZ UDPMincho":
		{
			"v": ["regular"],
			"s": ["cyrillic", "greek-ext", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Babylonica":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bad Script":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bahiana":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bahianita":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bai Jamjuree":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Bakbak One":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ballet":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Baloo 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["devanagari", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Bhai 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["gujarati", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Bhaijaan 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Bhaina 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "oriya", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Chettan 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "malayalam", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Da 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["bengali", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Paaji 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["gurmukhi", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Tamma 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["kannada", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Tammudu 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "telugu", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Baloo Thambi 2":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "tamil", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Balsamiq Sans":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Balthazar":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bangers":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Barlow":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Barlow Condensed":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Barlow Semi Condensed":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Barriecito":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Barrio":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Basic":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Baskervville":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Battambang":
		{
			"v": ["100", "300", "regular", "700", "900"],
			"s": ["khmer", "latin"],
			"w": ["100", "300", "regular", "700", "900"],
			"i": ["normal"]
		},
		"Baumans":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bayon":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Be Vietnam Pro":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Beau Rivage":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bebas Neue":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Belgrano":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bellefair":
		{
			"v": ["regular"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Belleza":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bellota":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Bellota Text":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"BenchNine":
		{
			"v": ["300", "regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Benne":
		{
			"v": ["regular"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bentham":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Berkshire Swash":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Besley":
		{
			"v": ["regular", "500", "600", "700", "800", "900", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800", "900", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Beth Ellen":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bevan":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"BhuTuka Expanded One":
		{
			"v": ["regular"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Big Shoulders Display":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Big Shoulders Inline Display":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Big Shoulders Inline Text":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Big Shoulders Stencil Display":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Big Shoulders Stencil Text":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Big Shoulders Text":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Bigelow Rules":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bigshot One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bilbo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bilbo Swash Caps":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"BioRhyme":
		{
			"v": ["200", "300", "regular", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "700", "800"],
			"i": ["normal"]
		},
		"BioRhyme Expanded":
		{
			"v": ["200", "300", "regular", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "700", "800"],
			"i": ["normal"]
		},
		"Birthstone":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Birthstone Bounce":
		{
			"v": ["regular", "500"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500"],
			"i": ["normal"]
		},
		"Biryani":
		{
			"v": ["200", "300", "regular", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Bitter":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Black And White Picture":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Black Han Sans":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Black Ops One":
		{
			"v": ["regular"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Blaka":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Blaka Hollow":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Blaka Ink":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Blinker":
		{
			"v": ["100", "200", "300", "regular", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Bodoni Moda":
		{
			"v": ["regular", "500", "600", "700", "800", "900", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800", "900", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Bokor":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bona Nova":
		{
			"v": ["regular", "italic", "700"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Bonbon":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bonheur Royale":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Boogaloo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bowlby One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bowlby One SC":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Brawler":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Bree Serif":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Brygada 1918":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Bubblegum Sans":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bubbler One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Buda":
		{
			"v": ["300"],
			"s": ["latin"],
			"w": ["300"],
			"i": ["normal"]
		},
		"Buenard":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Bungee":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bungee Hairline":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bungee Inline":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bungee Outline":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bungee Shade":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Bungee Spice":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Butcherman":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Butterfly Kids":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cabin":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Cabin Condensed":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Cabin Sketch":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Caesar Dressing":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cagliostro":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cairo":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Cairo Play":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Caladea":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Calistoga":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Calligraffitti":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cambay":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Cambo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Candal":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cantarell":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Cantata One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cantora One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Capriola":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Caramel":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Carattere":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cardo":
		{
			"v": ["regular", "italic", "700"],
			"s": ["greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Carme":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Carrois Gothic":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Carrois Gothic SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Carter One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Castoro":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Catamaran":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Caudex":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Caveat":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Caveat Brush":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cedarville Cursive":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ceviche One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Chakra Petch":
		{
			"v": ["300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Changa":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Changa One":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Chango":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Charis SIL":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Charm":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Charmonman":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Chathura":
		{
			"v": ["100", "300", "regular", "700", "800"],
			"s": ["latin", "telugu"],
			"w": ["100", "300", "regular", "700", "800"],
			"i": ["normal"]
		},
		"Chau Philomene One":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Chela One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Chelsea Market":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Chenla":
		{
			"v": ["regular"],
			"s": ["khmer"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cherish":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cherry Cream Soda":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cherry Swash":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Chewy":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Chicle":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Chilanka":
		{
			"v": ["regular"],
			"s": ["latin", "malayalam"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Chivo":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic", "900", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Chonburi":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cinzel":
		{
			"v": ["regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Cinzel Decorative":
		{
			"v": ["regular", "700", "900"],
			"s": ["latin"],
			"w": ["regular", "700", "900"],
			"i": ["normal"]
		},
		"Clicker Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Coda":
		{
			"v": ["regular", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "800"],
			"i": ["normal"]
		},
		"Coda Caption":
		{
			"v": ["800"],
			"s": ["latin", "latin-ext"],
			"w": ["800"],
			"i": ["normal"]
		},
		"Codystar":
		{
			"v": ["300", "regular"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular"],
			"i": ["normal"]
		},
		"Coiny":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "tamil", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Combo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Comfortaa":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Comforter":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Comforter Brush":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Comic Neue":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Coming Soon":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Commissioner":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Concert One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Condiment":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Content":
		{
			"v": ["regular", "700"],
			"s": ["khmer"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Contrail One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Convergence":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cookie":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Copse":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Corben":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Corinthia":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Cormorant":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Cormorant Garamond":
		{
			"v": ["300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Cormorant Infant":
		{
			"v": ["300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Cormorant SC":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Cormorant Unicase":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Cormorant Upright":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Courgette":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Courier Prime":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Cousine":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Coustard":
		{
			"v": ["regular", "900"],
			"s": ["latin"],
			"w": ["regular", "900"],
			"i": ["normal"]
		},
		"Covered By Your Grace":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Crafty Girls":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Creepster":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Crete Round":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Crimson Pro":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Crimson Text":
		{
			"v": ["regular", "italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Croissant One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Crushed":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cuprum":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Cute Font":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cutive":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Cutive Mono":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"DM Mono":
		{
			"v": ["300", "300italic", "regular", "italic", "500", "500italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500"],
			"i": ["normal", "italic"]
		},
		"DM Sans":
		{
			"v": ["regular", "italic", "500", "500italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "700"],
			"i": ["normal", "italic"]
		},
		"DM Serif Display":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"DM Serif Text":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Damion":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dancing Script":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Dangrek":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Darker Grotesque":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"David Libre":
		{
			"v": ["regular", "500", "700"],
			"s": ["hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "700"],
			"i": ["normal"]
		},
		"Dawning of a New Day":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Days One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dekko":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dela Gothic One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "greek", "japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Delius":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Delius Swash Caps":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Delius Unicase":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Della Respira":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Denk One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Devonshire":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dhurjati":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Didact Gothic":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Diplomata":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Diplomata SC":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Do Hyeon":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dokdo":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Domine":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Donegal One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dongle":
		{
			"v": ["300", "regular", "700"],
			"s": ["korean", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Doppio One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dorsa":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dosis":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"DotGothic16":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Dr Sugiyama":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Duru Sans":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"DynaPuff":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Dynalight":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"EB Garamond":
		{
			"v": ["regular", "500", "600", "700", "800", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Eagle Lake":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"East Sea Dokdo":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Eater":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Economica":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Eczar":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["devanagari", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Edu NSW ACT Foundation":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Edu QLD Beginner":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Edu SA Beginner":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Edu TAS Beginner":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Edu VIC WA NT Beginner":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"El Messiri":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic", "cyrillic", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Electrolize":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Elsie":
		{
			"v": ["regular", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "900"],
			"i": ["normal"]
		},
		"Elsie Swash Caps":
		{
			"v": ["regular", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "900"],
			"i": ["normal"]
		},
		"Emblema One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Emilys Candy":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Encode Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Encode Sans Condensed":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Encode Sans Expanded":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Encode Sans SC":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Encode Sans Semi Condensed":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Encode Sans Semi Expanded":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Engagement":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Englebert":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Enriqueta":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Ephesis":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Epilogue":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Erica One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Esteban":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Estonia":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Euphoria Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ewert":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Exo":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Exo 2":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Expletus Sans":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Explora":
		{
			"v": ["regular"],
			"s": ["cherokee", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fahkwang":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Familjen Grotesk":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Fanwood Text":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Farro":
		{
			"v": ["300", "regular", "500", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "700"],
			"i": ["normal"]
		},
		"Farsan":
		{
			"v": ["regular"],
			"s": ["gujarati", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fascinate":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fascinate Inline":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Faster One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fasthand":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fauna One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Faustina":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Federant":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Federo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Felipa":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fenix":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Festive":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Figtree":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Finger Paint":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Finlandica":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Fira Code":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Fira Mono":
		{
			"v": ["regular", "500", "700"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular", "500", "700"],
			"i": ["normal"]
		},
		"Fira Sans":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Fira Sans Condensed":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Fira Sans Extra Condensed":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Fjalla One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fjord One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Flamenco":
		{
			"v": ["300", "regular"],
			"s": ["latin"],
			"w": ["300", "regular"],
			"i": ["normal"]
		},
		"Flavors":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fleur De Leah":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Flow Block":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Flow Circular":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Flow Rounded":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fondamento":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Fontdiner Swanky":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Forum":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Francois One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Frank Ruhl Libre":
		{
			"v": ["300", "regular", "500", "700", "900"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Fraunces":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Freckle Face":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fredericka the Great":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fredoka":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Fredoka One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Freehand":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fresca":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Frijole":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fruktur":
		{
			"v": ["regular", "italic"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Fugaz One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fuggles":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Fuzzy Bubbles":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"GFS Didot":
		{
			"v": ["regular"],
			"s": ["greek"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"GFS Neohellenic":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["greek"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Gabriela":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gaegu":
		{
			"v": ["300", "regular", "700"],
			"s": ["korean", "latin"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Gafata":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Galada":
		{
			"v": ["regular"],
			"s": ["bengali", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Galdeano":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Galindo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gamja Flower":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gantari":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Gayathri":
		{
			"v": ["100", "regular", "700"],
			"s": ["latin", "malayalam"],
			"w": ["100", "regular", "700"],
			"i": ["normal"]
		},
		"Gelasio":
		{
			"v": ["regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Gemunu Libre":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "sinhala"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Genos":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cherokee", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Gentium Book Basic":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Gentium Book Plus":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Gentium Plus":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Geo":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Georama":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Geostar":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Geostar Fill":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Germania One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gideon Roman":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gidugu":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gilda Display":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Girassol":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Give You Glory":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Glass Antiqua":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Glegoo":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Gloria Hallelujah":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Glory":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Gluten":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Goblin One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gochi Hand":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Goldman":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Gorditas":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Gothic A1":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["korean", "latin"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Gotu":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Goudy Bookletter 1911":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gowun Batang":
		{
			"v": ["regular", "700"],
			"s": ["korean", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Gowun Dodum":
		{
			"v": ["regular"],
			"s": ["korean", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Graduate":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Grand Hotel":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Grandstander":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Grape Nuts":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gravitas One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Great Vibes":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Grechen Fuemen":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Grenze":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Grenze Gotisch":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Grey Qo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Griffy":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gruppo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gudea":
		{
			"v": ["regular", "italic", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Gugi":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gulzar":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gupter":
		{
			"v": ["regular", "500", "700"],
			"s": ["latin"],
			"w": ["regular", "500", "700"],
			"i": ["normal"]
		},
		"Gurajada":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Gwendolyn":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Habibi":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hachi Maru Pop":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hahmlet":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["korean", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Halant":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Hammersmith One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hanalei":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hanalei Fill":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Handlee":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hanuman":
		{
			"v": ["100", "300", "regular", "700", "900"],
			"s": ["khmer", "latin"],
			"w": ["100", "300", "regular", "700", "900"],
			"i": ["normal"]
		},
		"Happy Monkey":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Harmattan":
		{
			"v": ["regular", "700"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Headland One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Heebo":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["hebrew", "latin"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Henny Penny":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hepta Slab":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Herr Von Muellerhoff":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hi Melody":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hina Mincho":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hind":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Hind Guntur":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "telugu"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Hind Madurai":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Hind Siliguri":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Hind Vadodara":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Holtwood One SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Homemade Apple":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Homenaje":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hubballi":
		{
			"v": ["regular"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Hurricane":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"IBM Plex Mono":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"IBM Plex Sans":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"IBM Plex Sans Arabic":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["arabic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"IBM Plex Sans Condensed":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"IBM Plex Sans Devanagari":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["cyrillic-ext", "devanagari", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"IBM Plex Sans Hebrew":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"IBM Plex Sans KR":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["korean", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"IBM Plex Sans Thai":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "thai"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"IBM Plex Sans Thai Looped":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "thai"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"IBM Plex Serif":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"IM Fell DW Pica":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"IM Fell DW Pica SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"IM Fell Double Pica":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"IM Fell Double Pica SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"IM Fell English":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"IM Fell English SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"IM Fell French Canon":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"IM Fell French Canon SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"IM Fell Great Primer":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"IM Fell Great Primer SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ibarra Real Nova":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Iceberg":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Iceland":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Imbue":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Imperial Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Imprima":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Inconsolata":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Inder":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Indie Flower":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ingrid Darling":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Inika":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Inknut Antiqua":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Inria Sans":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Inria Serif":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Inspiration":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Inter":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Inter Tight":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Irish Grover":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Island Moments":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Istok Web":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Italiana":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Italianno":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Itim":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jacques Francois":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jacques Francois Shadow":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jaldi":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"JetBrains Mono":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Jim Nightshade":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Joan":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jockey One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jolly Lodger":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jomhuria":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jomolhari":
		{
			"v": ["regular"],
			"s": ["latin", "tibetan"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Josefin Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "200italic", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Josefin Slab":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "200italic", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Jost":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Joti One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jua":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Judson":
		{
			"v": ["regular", "italic", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Julee":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Julius Sans One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Junge":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Jura":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "kayah-li", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Just Another Hand":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Just Me Again Down Here":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"K2D":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal", "italic"]
		},
		"Kadwa":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Kaisei Decol":
		{
			"v": ["regular", "500", "700"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular", "500", "700"],
			"i": ["normal"]
		},
		"Kaisei HarunoUmi":
		{
			"v": ["regular", "500", "700"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular", "500", "700"],
			"i": ["normal"]
		},
		"Kaisei Opti":
		{
			"v": ["regular", "500", "700"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular", "500", "700"],
			"i": ["normal"]
		},
		"Kaisei Tokumin":
		{
			"v": ["regular", "500", "700", "800"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular", "500", "700", "800"],
			"i": ["normal"]
		},
		"Kalam":
		{
			"v": ["300", "regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Kameron":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Kanit":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Kantumruy":
		{
			"v": ["300", "regular", "700"],
			"s": ["khmer"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Kantumruy Pro":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["khmer", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "200italic", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Karantina":
		{
			"v": ["300", "regular", "700"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Karla":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "300italic", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Karma":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Katibeh":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kaushan Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kavivanar":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kavoon":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kdam Thmor Pro":
		{
			"v": ["regular"],
			"s": ["khmer", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Keania One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kelly Slab":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kenia":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Khand":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Khmer":
		{
			"v": ["regular"],
			"s": ["khmer"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Khula":
		{
			"v": ["300", "regular", "600", "700", "800"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "600", "700", "800"],
			"i": ["normal"]
		},
		"Kings":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kirang Haerang":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kite One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kiwi Maru":
		{
			"v": ["300", "regular", "500"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["300", "regular", "500"],
			"i": ["normal"]
		},
		"Klee One":
		{
			"v": ["regular", "600"],
			"s": ["cyrillic", "greek-ext", "japanese", "latin", "latin-ext"],
			"w": ["regular", "600"],
			"i": ["normal"]
		},
		"Knewave":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"KoHo":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Kodchasan":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Koh Santepheap":
		{
			"v": ["100", "300", "regular", "700", "900"],
			"s": ["khmer", "latin"],
			"w": ["100", "300", "regular", "700", "900"],
			"i": ["normal"]
		},
		"Kolker Brush":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kosugi":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kosugi Maru":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kotta One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Koulen":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kranky":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kreon":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Kristi":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Krona One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Krub":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Kufam":
		{
			"v": ["regular", "500", "600", "700", "800", "900", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "900", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Kulim Park":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Kumar One":
		{
			"v": ["regular"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kumar One Outline":
		{
			"v": ["regular"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Kumbh Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Kurale":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"La Belle Aurore":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lacquer":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Laila":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Lakki Reddy":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lalezar":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lancelot":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Langar":
		{
			"v": ["regular"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lateef":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Lato":
		{
			"v": ["100", "100italic", "300", "300italic", "regular", "italic", "700", "700italic", "900", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "300", "regular", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Lavishly Yours":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"League Gothic":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"League Script":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"League Spartan":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Leckerli One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ledger":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lekton":
		{
			"v": ["regular", "italic", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Lemon":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lemonada":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Lexend":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Deca":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Exa":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Giga":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Mega":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Peta":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Tera":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Lexend Zetta":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Libre Barcode 128":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Barcode 128 Text":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Barcode 39":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Barcode 39 Extended":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Barcode 39 Extended Text":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Barcode 39 Text":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Barcode EAN13 Text":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Baskerville":
		{
			"v": ["regular", "italic", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Libre Bodoni":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Libre Caslon Display":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Libre Caslon Text":
		{
			"v": ["regular", "italic", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Libre Franklin":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Licorice":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Life Savers":
		{
			"v": ["regular", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700", "800"],
			"i": ["normal"]
		},
		"Lilita One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lily Script One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Limelight":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Linden Hill":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Literata":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Liu Jian Mao Cao":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Livvic":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Lobster":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lobster Two":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Londrina Outline":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Londrina Shadow":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Londrina Sketch":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Londrina Solid":
		{
			"v": ["100", "300", "regular", "900"],
			"s": ["latin"],
			"w": ["100", "300", "regular", "900"],
			"i": ["normal"]
		},
		"Long Cang":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lora":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Love Light":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Love Ya Like A Sister":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Loved by the King":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lovers Quarrel":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Luckiest Guy":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Lusitana":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Lustria":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Luxurious Roman":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Luxurious Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"M PLUS 1":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"M PLUS 1 Code":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"M PLUS 1p":
		{
			"v": ["100", "300", "regular", "500", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "hebrew", "japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "300", "regular", "500", "700", "800", "900"],
			"i": ["normal"]
		},
		"M PLUS 2":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"M PLUS Code Latin":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"M PLUS Rounded 1c":
		{
			"v": ["100", "300", "regular", "500", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "hebrew", "japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "300", "regular", "500", "700", "800", "900"],
			"i": ["normal"]
		},
		"Ma Shan Zheng":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Macondo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Macondo Swash Caps":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mada":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "900"],
			"s": ["arabic", "latin"],
			"w": ["200", "300", "regular", "500", "600", "700", "900"],
			"i": ["normal"]
		},
		"Magra":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Maiden Orange":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Maitree":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Major Mono Display":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mako":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mali":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Mallanna":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mandali":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Manjari":
		{
			"v": ["100", "regular", "700"],
			"s": ["latin", "latin-ext", "malayalam"],
			"w": ["100", "regular", "700"],
			"i": ["normal"]
		},
		"Manrope":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Mansalva":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Manuale":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Marcellus":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Marcellus SC":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Marck Script":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Margarine":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Markazi Text":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Marko One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Marmelad":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Martel":
		{
			"v": ["200", "300", "regular", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Martel Sans":
		{
			"v": ["200", "300", "regular", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Marvel":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Mate":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Mate SC":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Maven Pro":
		{
			"v": ["regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"McLaren":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mea Culpa":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Meddon":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"MedievalSharp":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Medula One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Meera Inimai":
		{
			"v": ["regular"],
			"s": ["latin", "tamil"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Megrim":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Meie Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Meow Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Merienda":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Merienda One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Merriweather":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Merriweather Sans":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Metal":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Metal Mania":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Metamorphous":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Metrophobic":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Michroma":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Milonga":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Miltonian":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Miltonian Tattoo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mina":
		{
			"v": ["regular", "700"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Mingzat":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "lepcha"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Miniver":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Miriam Libre":
		{
			"v": ["regular", "700"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Mirza":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Miss Fajardose":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mitr":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Mochiy Pop One":
		{
			"v": ["regular"],
			"s": ["japanese", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mochiy Pop P One":
		{
			"v": ["regular"],
			"s": ["japanese", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Modak":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Modern Antiqua":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mogra":
		{
			"v": ["regular"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mohave":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Molengo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Molle":
		{
			"v": ["italic"],
			"s": ["latin", "latin-ext"],
			"w": [],
			"i": ["normal", "italic"]
		},
		"Monda":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Monofett":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Monoton":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Monsieur La Doulaise":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Montaga":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Montagu Slab":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"MonteCarlo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Montez":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Montserrat":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Montserrat Alternates":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Montserrat Subrayada":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Moo Lah Lah":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Moon Dance":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Moul":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Moulpali":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mountains of Christmas":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Mouse Memoirs":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mr Bedfort":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mr Dafoe":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mr De Haviland":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mrs Saint Delafield":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mrs Sheppards":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ms Madi":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mukta":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Mukta Mahee":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Mukta Malar":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Mukta Vaani":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Mulish":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Murecho":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "japanese", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"MuseoModerno":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"My Soul":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Mystery Quest":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"NTR":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nabla":
		{
			"v": ["regular"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "math", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nanum Brush Script":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nanum Gothic":
		{
			"v": ["regular", "700", "800"],
			"s": ["korean", "latin"],
			"w": ["regular", "700", "800"],
			"i": ["normal"]
		},
		"Nanum Gothic Coding":
		{
			"v": ["regular", "700"],
			"s": ["korean", "latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Nanum Myeongjo":
		{
			"v": ["regular", "700", "800"],
			"s": ["korean", "latin"],
			"w": ["regular", "700", "800"],
			"i": ["normal"]
		},
		"Nanum Pen Script":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Neonderthaw":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nerko One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Neucha":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Neuton":
		{
			"v": ["200", "300", "regular", "italic", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "700", "800"],
			"i": ["normal", "italic"]
		},
		"New Rocker":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"New Tegomin":
		{
			"v": ["regular"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"News Cycle":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Newsreader":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "300italic", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Niconne":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Niramit":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Nixie One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nobile":
		{
			"v": ["regular", "italic", "500", "500italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "700"],
			"i": ["normal", "italic"]
		},
		"Nokora":
		{
			"v": ["100", "300", "regular", "700", "900"],
			"s": ["khmer", "latin"],
			"w": ["100", "300", "regular", "700", "900"],
			"i": ["normal"]
		},
		"Norican":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nosifer":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Notable":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nothing You Could Do":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noticia Text":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Noto Color Emoji":
		{
			"v": ["regular"],
			"s": ["emoji"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Emoji":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["emoji"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Kufi Arabic":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["arabic"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Music":
		{
			"v": ["regular"],
			"s": ["music"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Naskh Arabic":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Nastaliq Urdu":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Rashi Hebrew":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "devanagari", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Noto Sans Adlam":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["adlam", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Adlam Unjoined":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["adlam", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Anatolian Hieroglyphs":
		{
			"v": ["regular"],
			"s": ["anatolian-hieroglyphs"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Arabic":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["arabic"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Armenian":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["armenian", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Avestan":
		{
			"v": ["regular"],
			"s": ["avestan", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Balinese":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["balinese", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Bamum":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["bamum"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Bassa Vah":
		{
			"v": ["regular"],
			"s": ["bassa-vah"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Batak":
		{
			"v": ["regular"],
			"s": ["batak", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Bengali":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Bhaiksuki":
		{
			"v": ["regular"],
			"s": ["bhaiksuki"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Brahmi":
		{
			"v": ["regular"],
			"s": ["brahmi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Buginese":
		{
			"v": ["regular"],
			"s": ["buginese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Buhid":
		{
			"v": ["regular"],
			"s": ["buhid", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Canadian Aboriginal":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["canadian-aboriginal", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Carian":
		{
			"v": ["regular"],
			"s": ["carian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Caucasian Albanian":
		{
			"v": ["regular"],
			"s": ["caucasian-albanian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Chakma":
		{
			"v": ["regular"],
			"s": ["chakma"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Cham":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cham", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Cherokee":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cherokee", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Coptic":
		{
			"v": ["regular"],
			"s": ["coptic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Cuneiform":
		{
			"v": ["regular"],
			"s": ["cuneiform"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Cypriot":
		{
			"v": ["regular"],
			"s": ["cypriot"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Deseret":
		{
			"v": ["regular"],
			"s": ["deseret"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Devanagari":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Display":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Noto Sans Duployan":
		{
			"v": ["regular"],
			"s": ["duployan"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Egyptian Hieroglyphs":
		{
			"v": ["regular"],
			"s": ["egyptian-hieroglyphs"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Elbasan":
		{
			"v": ["regular"],
			"s": ["elbasan"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Elymaic":
		{
			"v": ["regular"],
			"s": ["elymaic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Ethiopic":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["ethiopic", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Georgian":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["georgian", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Glagolitic":
		{
			"v": ["regular"],
			"s": ["glagolitic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Gothic":
		{
			"v": ["regular"],
			"s": ["gothic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Grantha":
		{
			"v": ["regular"],
			"s": ["grantha", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Gujarati":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Gunjala Gondi":
		{
			"v": ["regular"],
			"s": ["gunjala-gondi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Gurmukhi":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans HK":
		{
			"v": ["100", "300", "regular", "500", "700", "900"],
			"s": ["chinese-hongkong", "latin"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Noto Sans Hanifi Rohingya":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["hanifi-rohingya"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Hanunoo":
		{
			"v": ["regular"],
			"s": ["hanunoo"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Hatran":
		{
			"v": ["regular"],
			"s": ["hatran"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Hebrew":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Imperial Aramaic":
		{
			"v": ["regular"],
			"s": ["imperial-aramaic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Indic Siyaq Numbers":
		{
			"v": ["regular"],
			"s": ["indic-siyaq-numbers"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Inscriptional Pahlavi":
		{
			"v": ["regular"],
			"s": ["inscriptional-pahlavi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Inscriptional Parthian":
		{
			"v": ["regular"],
			"s": ["inscriptional-parthian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans JP":
		{
			"v": ["100", "300", "regular", "500", "700", "900"],
			"s": ["japanese", "latin"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Noto Sans Javanese":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["javanese", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans KR":
		{
			"v": ["100", "300", "regular", "500", "700", "900"],
			"s": ["korean", "latin"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Noto Sans Kaithi":
		{
			"v": ["regular"],
			"s": ["kaithi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Kannada":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Kayah Li":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["kayah-li"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Kharoshthi":
		{
			"v": ["regular"],
			"s": ["kharoshthi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Khmer":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["khmer", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Khojki":
		{
			"v": ["regular"],
			"s": ["khojki"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Khudawadi":
		{
			"v": ["regular"],
			"s": ["khudawadi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Lao":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["lao", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Lepcha":
		{
			"v": ["regular"],
			"s": ["lepcha"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Limbu":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "limbu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Linear A":
		{
			"v": ["regular"],
			"s": ["linear-a"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Linear B":
		{
			"v": ["regular"],
			"s": ["linear-b"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Lisu":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "lisu"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Lycian":
		{
			"v": ["regular"],
			"s": ["lycian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Lydian":
		{
			"v": ["regular"],
			"s": ["lydian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Mahajani":
		{
			"v": ["regular"],
			"s": ["mahajani"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Malayalam":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "malayalam"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Mandaic":
		{
			"v": ["regular"],
			"s": ["mandaic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Manichaean":
		{
			"v": ["regular"],
			"s": ["manichaean"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Marchen":
		{
			"v": ["regular"],
			"s": ["marchen"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Masaram Gondi":
		{
			"v": ["regular"],
			"s": ["masaram-gondi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Math":
		{
			"v": ["regular"],
			"s": ["math"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Mayan Numerals":
		{
			"v": ["regular"],
			"s": ["mayan-numerals"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Medefaidrin":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["medefaidrin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Meetei Mayek":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "meetei-mayek"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Meroitic":
		{
			"v": ["regular"],
			"s": ["meroitic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Miao":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "miao"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Modi":
		{
			"v": ["regular"],
			"s": ["modi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Mongolian":
		{
			"v": ["regular"],
			"s": ["mongolian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Mono":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Mro":
		{
			"v": ["regular"],
			"s": ["mro"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Multani":
		{
			"v": ["regular"],
			"s": ["multani"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Myanmar":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["myanmar"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans N Ko":
		{
			"v": ["regular"],
			"s": ["nko"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Nabataean":
		{
			"v": ["regular"],
			"s": ["nabataean"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans New Tai Lue":
		{
			"v": ["regular"],
			"s": ["new-tai-lue"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Newa":
		{
			"v": ["regular"],
			"s": ["newa"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Nushu":
		{
			"v": ["regular"],
			"s": ["nushu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Ogham":
		{
			"v": ["regular"],
			"s": ["ogham"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Ol Chiki":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["ol-chiki"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Old Hungarian":
		{
			"v": ["regular"],
			"s": ["old-hungarian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old Italic":
		{
			"v": ["regular"],
			"s": ["old-italic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old North Arabian":
		{
			"v": ["regular"],
			"s": ["old-north-arabian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old Permic":
		{
			"v": ["regular"],
			"s": ["old-permic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old Persian":
		{
			"v": ["regular"],
			"s": ["old-persian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old Sogdian":
		{
			"v": ["regular"],
			"s": ["old-sogdian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old South Arabian":
		{
			"v": ["regular"],
			"s": ["old-south-arabian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Old Turkic":
		{
			"v": ["regular"],
			"s": ["old-turkic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Oriya":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "oriya"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Osage":
		{
			"v": ["regular"],
			"s": ["osage"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Osmanya":
		{
			"v": ["regular"],
			"s": ["osmanya"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Pahawh Hmong":
		{
			"v": ["regular"],
			"s": ["pahawh-hmong"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Palmyrene":
		{
			"v": ["regular"],
			"s": ["palmyrene"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Pau Cin Hau":
		{
			"v": ["regular"],
			"s": ["pau-cin-hau"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Phags Pa":
		{
			"v": ["regular"],
			"s": ["phags-pa"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Phoenician":
		{
			"v": ["regular"],
			"s": ["phoenician"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Psalter Pahlavi":
		{
			"v": ["regular"],
			"s": ["psalter-pahlavi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Rejang":
		{
			"v": ["regular"],
			"s": ["rejang"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Runic":
		{
			"v": ["regular"],
			"s": ["runic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans SC":
		{
			"v": ["100", "300", "regular", "500", "700", "900"],
			"s": ["chinese-simplified", "latin"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Noto Sans Samaritan":
		{
			"v": ["regular"],
			"s": ["samaritan"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Saurashtra":
		{
			"v": ["regular"],
			"s": ["saurashtra"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Sharada":
		{
			"v": ["regular"],
			"s": ["sharada"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Shavian":
		{
			"v": ["regular"],
			"s": ["shavian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Siddham":
		{
			"v": ["regular"],
			"s": ["siddham"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Sinhala":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "sinhala"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Sogdian":
		{
			"v": ["regular"],
			"s": ["sogdian"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Sora Sompeng":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["sora-sompeng"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Soyombo":
		{
			"v": ["regular"],
			"s": ["soyombo"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Sundanese":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["sundanese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Syloti Nagri":
		{
			"v": ["regular"],
			"s": ["syloti-nagri"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Symbols":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "symbols"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Symbols 2":
		{
			"v": ["regular"],
			"s": ["symbols"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Syriac":
		{
			"v": ["100", "regular", "900"],
			"s": ["syriac"],
			"w": ["100", "regular", "900"],
			"i": ["normal"]
		},
		"Noto Sans TC":
		{
			"v": ["100", "300", "regular", "500", "700", "900"],
			"s": ["chinese-traditional", "latin"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Noto Sans Tagalog":
		{
			"v": ["regular"],
			"s": ["tagalog"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Tagbanwa":
		{
			"v": ["regular"],
			"s": ["tagbanwa"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Tai Le":
		{
			"v": ["regular"],
			"s": ["tai-le"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Tai Tham":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["tai-tham"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Sans Tai Viet":
		{
			"v": ["regular"],
			"s": ["tai-viet"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Takri":
		{
			"v": ["regular"],
			"s": ["takri"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Tamil":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Tamil Supplement":
		{
			"v": ["regular"],
			"s": ["tamil-supplement"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Telugu":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "telugu"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Thaana":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["thaana"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Thai":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "thai"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Thai Looped":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["thai"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Sans Tifinagh":
		{
			"v": ["regular"],
			"s": ["tifinagh"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Tirhuta":
		{
			"v": ["regular"],
			"s": ["tirhuta"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Ugaritic":
		{
			"v": ["regular"],
			"s": ["ugaritic"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Vai":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vai"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Wancho":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "wancho"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Warang Citi":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "warang-citi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Yi":
		{
			"v": ["regular"],
			"s": ["yi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Sans Zanabazar Square":
		{
			"v": ["regular"],
			"s": ["zanabazar-square"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Serif":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Noto Serif Ahom":
		{
			"v": ["regular"],
			"s": ["ahom", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Serif Armenian":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["armenian", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Balinese":
		{
			"v": ["regular"],
			"s": ["balinese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Serif Bengali":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Devanagari":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Display":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Noto Serif Dogra":
		{
			"v": ["regular"],
			"s": ["dogra"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Serif Ethiopic":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["ethiopic", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Georgian":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["georgian", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Grantha":
		{
			"v": ["regular"],
			"s": ["grantha", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Serif Gujarati":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Gurmukhi":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif HK":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["chinese-hongkong", "cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Hebrew":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif JP":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "900"],
			"s": ["japanese", "latin"],
			"w": ["200", "300", "regular", "500", "600", "700", "900"],
			"i": ["normal"]
		},
		"Noto Serif KR":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "900"],
			"s": ["korean", "latin"],
			"w": ["200", "300", "regular", "500", "600", "700", "900"],
			"i": ["normal"]
		},
		"Noto Serif Kannada":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Khmer":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["khmer", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Lao":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["lao", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Malayalam":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "malayalam"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Myanmar":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["myanmar"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Nyiakeng Puachue Hmong":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["nyiakeng-puachue-hmong"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Serif SC":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "900"],
			"s": ["chinese-simplified", "latin"],
			"w": ["200", "300", "regular", "500", "600", "700", "900"],
			"i": ["normal"]
		},
		"Noto Serif Sinhala":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "sinhala"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif TC":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "900"],
			"s": ["chinese-traditional", "latin"],
			"w": ["200", "300", "regular", "500", "600", "700", "900"],
			"i": ["normal"]
		},
		"Noto Serif Tamil":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Noto Serif Tangut":
		{
			"v": ["regular"],
			"s": ["tangut"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Noto Serif Telugu":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "telugu"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Thai":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "thai"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Tibetan":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["tibetan"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Noto Serif Yezidi":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["yezidi"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Noto Traditional Nushu":
		{
			"v": ["regular"],
			"s": ["nushu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Cut":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Flat":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Mono":
		{
			"v": ["regular"],
			"s": ["greek", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Oval":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Round":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Script":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Slim":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nova Square":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Numans":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Nunito":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Nunito Sans":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Nuosu SIL":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "yi"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Odibee Sans":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Odor Mean Chey":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Offside":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Oi":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "tamil", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Old Standard TT":
		{
			"v": ["regular", "italic", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Oldenburg":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ole":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Oleo Script":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Oleo Script Swash Caps":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Oooh Baby":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Open Sans":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Oranienbaum":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Orbitron":
		{
			"v": ["regular", "500", "600", "700", "800", "900"],
			"s": ["latin"],
			"w": ["regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Oregano":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Orelega One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Orienta":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Original Surfer":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Oswald":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Outfit":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Over the Rainbow":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Overlock":
		{
			"v": ["regular", "italic", "700", "700italic", "900", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Overlock SC":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Overpass":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Overpass Mono":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Ovo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Oxanium":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Oxygen":
		{
			"v": ["300", "regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Oxygen Mono":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"PT Mono":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"PT Sans":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"PT Sans Caption":
		{
			"v": ["regular", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"PT Sans Narrow":
		{
			"v": ["regular", "700"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"PT Serif":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"PT Serif Caption":
		{
			"v": ["regular", "italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Pacifico":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Padauk":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "myanmar"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Palanquin":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Palanquin Dark":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Pangolin":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Paprika":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Parisienne":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Passero One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Passion One":
		{
			"v": ["regular", "700", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700", "900"],
			"i": ["normal"]
		},
		"Passions Conflict":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pathway Gothic One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Patrick Hand":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Patrick Hand SC":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pattaya":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Patua One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pavanam":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Paytone One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Peddana":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Peralta":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Permanent Marker":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Petemoss":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Petit Formal Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Petrona":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Philosopher":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Piazzolla":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Piedra":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pinyon Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pirata One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Plaster":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Play":
		{
			"v": ["regular", "700"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Playball":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Playfair Display":
		{
			"v": ["regular", "500", "600", "700", "800", "900", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "900", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Playfair Display SC":
		{
			"v": ["regular", "italic", "700", "700italic", "900", "900italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Plus Jakarta Sans":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "300italic", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Podkova":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Poiret One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Poller One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Poly":
		{
			"v": ["regular", "italic"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Pompiere":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pontano Sans":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Poor Story":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Poppins":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Port Lligat Sans":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Port Lligat Slab":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Potta One":
		{
			"v": ["regular"],
			"s": ["japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pragati Narrow":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Praise":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Prata":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Preahvihear":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Press Start 2P":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Pridi":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Princess Sofia":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Prociono":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Prompt":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Prosto One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Proza Libre":
		{
			"v": ["regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal", "italic"]
		},
		"Public Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Puppies Play":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Puritan":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Purple Purse":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Qahiri":
		{
			"v": ["regular"],
			"s": ["arabic", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Quando":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Quantico":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Quattrocento":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Quattrocento Sans":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Questrial":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Quicksand":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Quintessential":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Qwigley":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Qwitcher Grypen":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Racing Sans One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Radio Canada":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Radley":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Rajdhani":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Rakkas":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Raleway":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Raleway Dots":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ramabhadra":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ramaraja":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rambla":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Rammetto One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rampart One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ranchers":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rancho":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ranga":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Rasa":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["gujarati", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Rationale":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ravi Prakash":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Readex Pro":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Recursive":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Red Hat Display":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "800", "900", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Red Hat Mono":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Red Hat Text":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Red Rose":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Redacted":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Redacted Script":
		{
			"v": ["300", "regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Redressed":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Reem Kufi":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic", "latin"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Reem Kufi Fun":
		{
			"v": ["regular", "500", "600", "700"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Reem Kufi Ink":
		{
			"v": ["regular"],
			"s": ["arabic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Reenie Beanie":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Reggae One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Revalia":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rhodium Libre":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ribeye":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ribeye Marrow":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Righteous":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Risque":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Road Rage":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Roboto":
		{
			"v": ["100", "100italic", "300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Roboto Condensed":
		{
			"v": ["300", "300italic", "regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "700"],
			"i": ["normal", "italic"]
		},
		"Roboto Flex":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Roboto Mono":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "200italic", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Roboto Serif":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Roboto Slab":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Rochester":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rock Salt":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"RocknRoll One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rokkitt":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Romanesco":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ropa Sans":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Rosario":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Rosarivo":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Rouge Script":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rowdies":
		{
			"v": ["300", "regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Rozha One":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "800", "900", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Rubik Beastly":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Bubbles":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Burned":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Dirt":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Distressed":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Glitch":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Iso":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Marker Hatch":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Maze":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Microbe":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Mono One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Moonrocks":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Puddles":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rubik Wet Paint":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ruda":
		{
			"v": ["regular", "500", "600", "700", "800", "900"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Rufina":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Ruge Boogie":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ruluko":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rum Raisin":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ruslan Display":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Russo One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ruthie":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Rye":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"STIX Two Text":
		{
			"v": ["regular", "500", "600", "700", "italic", "500italic", "600italic", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "500italic", "700italic"],
			"i": ["normal", "italic"]
		},
		"Sacramento":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sahitya":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Sail":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Saira":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Saira Condensed":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Saira Extra Condensed":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Saira Semi Condensed":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Saira Stencil One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Salsa":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sanchez":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Sancreek":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sansita":
		{
			"v": ["regular", "italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Sansita Swashed":
		{
			"v": ["300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Sarabun":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal", "italic"]
		},
		"Sarala":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Sarina":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sarpanch":
		{
			"v": ["regular", "500", "600", "700", "800", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Sassy Frass":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Satisfy":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sawarabi Gothic":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sawarabi Mincho":
		{
			"v": ["regular"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Scada":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Scheherazade New":
		{
			"v": ["regular", "700"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Schoolbell":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Scope One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Seaweed Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Secular One":
		{
			"v": ["regular"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sedgwick Ave":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sedgwick Ave Display":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sen":
		{
			"v": ["regular", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700", "800"],
			"i": ["normal"]
		},
		"Send Flowers":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sevillana":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Seymour One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shadows Into Light":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shadows Into Light Two":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shalimar":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shanti":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Share":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Share Tech":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Share Tech Mono":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shippori Antique":
		{
			"v": ["regular"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shippori Antique B1":
		{
			"v": ["regular"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shippori Mincho":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Shippori Mincho B1":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Shojumaru":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Short Stack":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Shrikhand":
		{
			"v": ["regular"],
			"s": ["gujarati", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Siemreap":
		{
			"v": ["regular"],
			"s": ["khmer"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sigmar One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Signika":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Signika Negative":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Silkscreen":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Simonetta":
		{
			"v": ["regular", "italic", "900", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "900"],
			"i": ["normal", "italic"]
		},
		"Single Day":
		{
			"v": ["regular"],
			"s": ["korean"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sintony":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Sirin Stencil":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Six Caps":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Skranji":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Slabo 13px":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Slabo 27px":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Slackey":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Smokum":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Smooch":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Smooch Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Smythe":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sniglet":
		{
			"v": ["regular", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "800"],
			"i": ["normal"]
		},
		"Snippet":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Snowburst One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sofadi One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sofia":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Solway":
		{
			"v": ["300", "regular", "500", "700", "800"],
			"s": ["latin"],
			"w": ["300", "regular", "500", "700", "800"],
			"i": ["normal"]
		},
		"Song Myung":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sonsie One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sora":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Sorts Mill Goudy":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Source Code Pro":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Source Sans 3":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Source Sans Pro":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "600", "600italic", "700", "700italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "600", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Source Serif 4":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800", "900", "300italic", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Source Serif Pro":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "600", "600italic", "700", "700italic", "900", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "600", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Space Grotesk":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Space Mono":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Special Elite":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Spectral":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal", "italic"]
		},
		"Spectral SC":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal", "italic"]
		},
		"Spicy Rice":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Spinnaker":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Spirax":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Splash":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Spline Sans":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Spline Sans Mono":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Squada One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Square Peg":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sree Krushnadevaraya":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sriracha":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Srisakdi":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Staatliches":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stalemate":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stalinist One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stardos Stencil":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Stick":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stick No Bills":
		{
			"v": ["200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "sinhala"],
			"w": ["200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Stint Ultra Condensed":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stint Ultra Expanded":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stoke":
		{
			"v": ["300", "regular"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular"],
			"i": ["normal"]
		},
		"Strait":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Style Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Stylish":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sue Ellen Francisco":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Suez One":
		{
			"v": ["regular"],
			"s": ["hebrew", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sulphur Point":
		{
			"v": ["300", "regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "700"],
			"i": ["normal"]
		},
		"Sumana":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Sunflower":
		{
			"v": ["300", "500", "700"],
			"s": ["korean", "latin"],
			"w": ["300", "500", "700"],
			"i": ["normal"]
		},
		"Sunshiney":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Supermercado One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Sura":
		{
			"v": ["regular", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Suranna":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Suravaram":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Suwannaphum":
		{
			"v": ["100", "300", "regular", "700", "900"],
			"s": ["khmer", "latin"],
			"w": ["100", "300", "regular", "700", "900"],
			"i": ["normal"]
		},
		"Swanky and Moo Moo":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Syncopate":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Syne":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["greek", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Syne Mono":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Syne Tactile":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Tai Heritage Pro":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "tai-viet", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Tajawal":
		{
			"v": ["200", "300", "regular", "500", "700", "800", "900"],
			"s": ["arabic", "latin"],
			"w": ["200", "300", "regular", "500", "700", "800", "900"],
			"i": ["normal"]
		},
		"Tangerine":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Tapestry":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Taprom":
		{
			"v": ["regular"],
			"s": ["khmer", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Tauri":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Taviraj":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Teko":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Telex":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Tenali Ramakrishna":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Tenor Sans":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Text Me One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Texturina":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Thasadith":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"The Girl Next Door":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"The Nautigal":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Tienne":
		{
			"v": ["regular", "700", "900"],
			"s": ["latin"],
			"w": ["regular", "700", "900"],
			"i": ["normal"]
		},
		"Tillana":
		{
			"v": ["regular", "500", "600", "700", "800"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Timmana":
		{
			"v": ["regular"],
			"s": ["latin", "telugu"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Tinos":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Tiro Bangla":
		{
			"v": ["regular", "italic"],
			"s": ["bengali", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Devanagari Hindi":
		{
			"v": ["regular", "italic"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Devanagari Marathi":
		{
			"v": ["regular", "italic"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Devanagari Sanskrit":
		{
			"v": ["regular", "italic"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Gurmukhi":
		{
			"v": ["regular", "italic"],
			"s": ["gurmukhi", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Kannada":
		{
			"v": ["regular", "italic"],
			"s": ["kannada", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Tamil":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext", "tamil"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Tiro Telugu":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext", "telugu"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Titan One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Titillium Web":
		{
			"v": ["200", "200italic", "300", "300italic", "regular", "italic", "600", "600italic", "700", "700italic", "900"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "600", "700", "900"],
			"i": ["normal", "italic"]
		},
		"Tomorrow":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Tourney":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Trade Winds":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Train One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Trirong":
		{
			"v": ["100", "100italic", "200", "200italic", "300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic", "800", "800italic", "900", "900italic"],
			"s": ["latin", "latin-ext", "thai", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal", "italic"]
		},
		"Trispace":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800"],
			"i": ["normal"]
		},
		"Trocchi":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Trochut":
		{
			"v": ["regular", "italic", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Truculenta":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Trykker":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Tulpen One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Turret Road":
		{
			"v": ["200", "300", "regular", "500", "700", "800"],
			"s": ["latin", "latin-ext"],
			"w": ["200", "300", "regular", "500", "700", "800"],
			"i": ["normal"]
		},
		"Twinkle Star":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ubuntu":
		{
			"v": ["300", "300italic", "regular", "italic", "500", "500italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "700"],
			"i": ["normal", "italic"]
		},
		"Ubuntu Condensed":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ubuntu Mono":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "greek-ext", "latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Uchen":
		{
			"v": ["regular"],
			"s": ["latin", "tibetan"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Ultra":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Uncial Antiqua":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Underdog":
		{
			"v": ["regular"],
			"s": ["cyrillic", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Unica One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"UnifrakturCook":
		{
			"v": ["700"],
			"s": ["latin"],
			"w": ["700"],
			"i": ["normal"]
		},
		"UnifrakturMaguntia":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Unkempt":
		{
			"v": ["regular", "700"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal"]
		},
		"Unlock":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Unna":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Updock":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Urbanist":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"VT323":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Vampiro One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Varela":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Varela Round":
		{
			"v": ["regular"],
			"s": ["hebrew", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Varta":
		{
			"v": ["300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Vast Shadow":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Vazirmatn":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"s": ["arabic", "latin", "latin-ext"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900"],
			"i": ["normal"]
		},
		"Vesper Libre":
		{
			"v": ["regular", "500", "700", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Viaoda Libre":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Vibes":
		{
			"v": ["regular"],
			"s": ["arabic", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Vibur":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Vidaloka":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Viga":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Voces":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Volkhov":
		{
			"v": ["regular", "italic", "700", "700italic"],
			"s": ["latin"],
			"w": ["regular", "700"],
			"i": ["normal", "italic"]
		},
		"Vollkorn":
		{
			"v": ["regular", "500", "600", "700", "800", "900", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["cyrillic", "cyrillic-ext", "greek", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500", "600", "700", "800", "900", "500italic", "700italic", "900italic"],
			"i": ["normal", "italic"]
		},
		"Vollkorn SC":
		{
			"v": ["regular", "600", "700", "900"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular", "600", "700", "900"],
			"i": ["normal"]
		},
		"Voltaire":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Vujahday Script":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Waiting for the Sunrise":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Wallpoet":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Walter Turncoat":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Warnes":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Water Brush":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Waterfall":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Wellfleet":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Wendy One":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Whisper":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"WindSong":
		{
			"v": ["regular", "500"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular", "500"],
			"i": ["normal"]
		},
		"Wire One":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Work Sans":
		{
			"v": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "100italic", "200italic", "300italic", "italic", "500italic", "600italic", "700italic", "800italic", "900italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["100", "200", "300", "regular", "500", "600", "700", "800", "900", "200italic", "italic", "600italic", "800italic"],
			"i": ["normal", "italic"]
		},
		"Xanh Mono":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Yaldevi":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["latin", "latin-ext", "sinhala"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Yanone Kaffeesatz":
		{
			"v": ["200", "300", "regular", "500", "600", "700"],
			"s": ["cyrillic", "latin", "latin-ext", "vietnamese"],
			"w": ["200", "300", "regular", "500", "600", "700"],
			"i": ["normal"]
		},
		"Yantramanav":
		{
			"v": ["100", "300", "regular", "500", "700", "900"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["100", "300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Yatra One":
		{
			"v": ["regular"],
			"s": ["devanagari", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yellowtail":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yeon Sung":
		{
			"v": ["regular"],
			"s": ["korean", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yeseva One":
		{
			"v": ["regular"],
			"s": ["cyrillic", "cyrillic-ext", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yesteryear":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yomogi":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext", "vietnamese"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yrsa":
		{
			"v": ["300", "regular", "500", "600", "700", "300italic", "italic", "500italic", "600italic", "700italic"],
			"s": ["latin", "latin-ext", "vietnamese"],
			"w": ["300", "regular", "500", "600", "700", "italic", "600italic"],
			"i": ["normal", "italic"]
		},
		"Yuji Boku":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yuji Mai":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yuji Syuku":
		{
			"v": ["regular"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Yusei Magic":
		{
			"v": ["regular"],
			"s": ["japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"ZCOOL KuaiLe":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"ZCOOL QingKe HuangYou":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"ZCOOL XiaoWei":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zen Antique":
		{
			"v": ["regular"],
			"s": ["cyrillic", "greek", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zen Antique Soft":
		{
			"v": ["regular"],
			"s": ["cyrillic", "greek", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zen Dots":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zen Kaku Gothic Antique":
		{
			"v": ["300", "regular", "500", "700", "900"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Zen Kaku Gothic New":
		{
			"v": ["300", "regular", "500", "700", "900"],
			"s": ["cyrillic", "japanese", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Zen Kurenaido":
		{
			"v": ["regular"],
			"s": ["cyrillic", "greek", "japanese", "latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zen Loop":
		{
			"v": ["regular", "italic"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal", "italic"]
		},
		"Zen Maru Gothic":
		{
			"v": ["300", "regular", "500", "700", "900"],
			"s": ["cyrillic", "greek", "japanese", "latin", "latin-ext"],
			"w": ["300", "regular", "500", "700", "900"],
			"i": ["normal"]
		},
		"Zen Old Mincho":
		{
			"v": ["regular", "700", "900"],
			"s": ["cyrillic", "greek", "japanese", "latin", "latin-ext"],
			"w": ["regular", "700", "900"],
			"i": ["normal"]
		},
		"Zen Tokyo Zoo":
		{
			"v": ["regular"],
			"s": ["latin", "latin-ext"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zeyada":
		{
			"v": ["regular"],
			"s": ["latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zhi Mang Xing":
		{
			"v": ["regular"],
			"s": ["chinese-simplified", "latin"],
			"w": ["regular"],
			"i": ["normal"]
		},
		"Zilla Slab":
		{
			"v": ["300", "300italic", "regular", "italic", "500", "500italic", "600", "600italic", "700", "700italic"],
			"s": ["latin", "latin-ext"],
			"w": ["300", "regular", "500", "600", "700"],
			"i": ["normal", "italic"]
		},
		"Zilla Slab Highlight":
		{
			"v": ["regular", "700"],
			"s": ["latin", "latin-ext"],
			"w": ["regular", "700"],
			"i": ["normal"]
		}
	}
}